


using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

public class MicroservicesTopologyAnalyzer
{
    public static async Task Main(string[] args)
    {
        var sourceDirectoryPath = args[0]; // Path to directory containing .cs files for WebApiHost
        var contractsPath = args[1]; // Path to Domain.Service.Contracts.dll
        var appServicesPath = args[2]; // Path to Domain.Service.AppServices.dll

        // Gather all C# files in the directory
        var sourceFiles = Directory.GetFiles(sourceDirectoryPath, "*.cs", SearchOption.AllDirectories);
        var syntaxTrees = sourceFiles.Select(file => CSharpSyntaxTree.ParseText(File.ReadAllText(file))).ToList();

        // Create a compilation including the necessary references
        var references = new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(contractsPath),
            MetadataReference.CreateFromFile(appServicesPath)
        };
        var compilation = CSharpCompilation.Create("MicroservicesAnalysis")
            .AddSyntaxTrees(syntaxTrees)
            .AddReferences(references);

        // Analyze the syntax trees for method call mappings
        var callMappings = await AnalyzeSyntaxTrees(syntaxTrees, compilation);

        // Output the results
        foreach (var mapping in callMappings)
        {
            Console.WriteLine($"{mapping.ControllerMethod} calls {mapping.HandlerMethod} which calls {mapping.CalledExternalMethod}");
        }
    }

    private static async Task<List<CallMapping>> AnalyzeSyntaxTrees(IEnumerable<SyntaxTree> syntaxTrees, Compilation compilation)
    {
        var mappings = new List<CallMapping>();

        foreach (var syntaxTree in syntaxTrees)
        {
            var model = compilation.GetSemanticModel(syntaxTree);

            // Find all method declarations in classes ending with "Controller"
            var controllerMethods = syntaxTree.GetRoot().DescendantNodes()
                .OfType<MethodDeclarationSyntax>()
                .Where(method => method.Parent is ClassDeclarationSyntax classDecl &&
                                 classDecl.Identifier.Text.EndsWith("Controller")); // Filter for controllers

            foreach (var controllerMethod in controllerMethods)
            {
                var controllerMethodSymbol = model.GetDeclaredSymbol(controllerMethod) as IMethodSymbol;
                var controllerMethodName = $"{controllerMethodSymbol.ContainingType}.{controllerMethodSymbol.Name}";

                // Find method invocations from the controller to handlers
                var handlerInvocations = FindMethodInvocations(controllerMethod, model);

                foreach (var handlerInvocation in handlerInvocations)
                {
                    var handlerMethodName = handlerInvocation.MethodName;

                    // Analyze the handler method for external contract calls
                    var externalCalls = FindExternalContractCalls(handlerInvocation.MethodSymbol, model);

                    foreach (var externalCall in externalCalls)
                    {
                        mappings.Add(new CallMapping
                        {
                            ControllerMethod = controllerMethodName,
                            HandlerMethod = handlerMethodName,
                            CalledExternalMethod = externalCall
                        });
                    }
                }
            }
        }

        return mappings;
    }

    private static List<MethodInvocation> FindMethodInvocations(MethodDeclarationSyntax methodDeclaration, SemanticModel model)
    {
        var invocations = new List<MethodInvocation>();

        var invocationExpressions = methodDeclaration.DescendantNodes()
            .OfType<InvocationExpressionSyntax>();

        foreach (var invocation in invocationExpressions)
        {
            var symbolInfo = model.GetSymbolInfo(invocation).Symbol as IMethodSymbol;

            if (symbolInfo != null)
            {
                var methodName = $"{symbolInfo.ContainingType}.{symbolInfo.Name}";
                invocations.Add(new MethodInvocation
                {
                    MethodName = methodName,
                    MethodSymbol = symbolInfo
                });
            }
        }

        return invocations;
    }

    private static List<string> FindExternalContractCalls(IMethodSymbol handlerMethodSymbol, SemanticModel model)
    {
        var externalCalls = new List<string>();

        // Analyze the syntax tree for the handler method's body
        var methodDeclarationSyntax = handlerMethodSymbol.DeclaringSyntaxReferences
            .FirstOrDefault()?.GetSyntax() as MethodDeclarationSyntax;

        if (methodDeclarationSyntax == null)
            return externalCalls;

        var invocationExpressions = methodDeclarationSyntax.DescendantNodes()
            .OfType<InvocationExpressionSyntax>();

        foreach (var invocation in invocationExpressions)
        {
            var symbolInfo = model.GetSymbolInfo(invocation).Symbol as IMethodSymbol;

            if (symbolInfo != null && symbolInfo.ContainingType.TypeKind == TypeKind.Interface)
            {
                var calledMethodName = $"{symbolInfo.ContainingType}.{symbolInfo.Name}";
                externalCalls.Add(calledMethodName);
            }
        }

        return externalCalls;
    }
}

public class CallMapping
{
    public string ControllerMethod { get; set; }
    public string HandlerMethod { get; set; }
    public string CalledExternalMethod { get; set; }
}

public class MethodInvocation
{
    public string MethodName { get; set; }
    public IMethodSymbol MethodSymbol { get; set; }
}







To correctly model the interactions between controllers and external services through handlers in the `AppServices` layer, the code analysis should account for the flow through three key components:

1. **Controllers in `WebApiHost`**: These controllers are the entry points that receive HTTP requests.
2. **Handlers in `AppServices`**: Controllers in `WebApiHost` use handlers defined in `AppServices` to perform the actual business logic.
3. **External Contracts**: Handlers in `AppServices` call methods from contracts/interfaces defined in other services (e.g., `DomainN.ServiceN.Contracts`).

The refined approach involves:

- **Analyzing Controller-to-Handler Calls**: Identify which handlers are being invoked by which controller methods.
- **Analyzing Handler-to-External Calls**: Identify which methods of external contracts (from `DomainN.ServiceN.Contracts`) are being called within the handler methods.
- **Constructing the Full Call Map**: Create a combined map showing that a controller method indirectly calls an external contract method via a handler.

### Updated Approach

1. **Load Assemblies and Parse Source Code**: Load the necessary assemblies and source files for `WebApiHost` and `AppServices`.
2. **Extract Method Call Chains**: 
   - Identify which methods in controllers call which methods in handlers.
   - Identify which methods in handlers call methods from external contracts.
3. **Combine Results**: For each controller method, link the handler it calls and, in turn, the external contracts that the handler invokes.
4. **Output the Results**: Produce a mapping that reflects the topology of method calls between controllers, handlers, and external contracts.

### Example Code in C#

Here is an updated version of the code analysis tool that takes this structure into account:

```csharp
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

public class MicroservicesTopologyAnalyzer
{
    public static void Main(string[] args)
    {
        var contractsPath = args[0]; // Path to Domain.Service.Contracts.dll
        var appServicesPath = args[1]; // Path to Domain.Service.AppServices.dll
        var webApiHostPath = args[2]; // Path to Domain.Service.WebApiHost.dll
        var webApiSourcePath = args[3]; // Path to the source code of WebApiHost

        // Load assemblies as metadata references
        var references = new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(contractsPath),
            MetadataReference.CreateFromFile(appServicesPath),
            MetadataReference.CreateFromFile(webApiHostPath)
        };

        // Analyze the WebApiHost and AppServices
        var callMappings = AnalyzeWebApiHostAndAppServices(webApiSourcePath, references);

        // Output the results
        foreach (var mapping in callMappings)
        {
            Console.WriteLine($"{mapping.ControllerMethod} calls {mapping.HandlerMethod} which calls {mapping.CalledExternalMethod}");
        }
    }

    private static List<CallMapping> AnalyzeWebApiHostAndAppServices(string sourcePath, MetadataReference[] references)
    {
        var sourceCode = File.ReadAllText(sourcePath);
        var syntaxTree = CSharpSyntaxTree.ParseText(sourceCode);
        var compilation = CSharpCompilation.Create("WebApiHostAnalysis")
            .AddSyntaxTrees(syntaxTree)
            .AddReferences(references);

        var model = compilation.GetSemanticModel(syntaxTree);

        var mappings = new List<CallMapping>();

        // Step 1: Find all method invocations in controllers
        var controllerMethods = syntaxTree.GetRoot().DescendantNodes()
            .OfType<MethodDeclarationSyntax>()
            .Where(method => method.Parent is ClassDeclarationSyntax classDecl &&
                             classDecl.Identifier.Text.EndsWith("Controller")); // Filter for controllers

        foreach (var controllerMethod in controllerMethods)
        {
            var controllerMethodSymbol = model.GetDeclaredSymbol(controllerMethod) as IMethodSymbol;
            var controllerMethodName = $"{controllerMethodSymbol.ContainingType}.{controllerMethodSymbol.Name}";

            // Find method invocations from the controller to handlers
            var handlerInvocations = FindMethodInvocations(controllerMethod, model);

            foreach (var handlerInvocation in handlerInvocations)
            {
                var handlerMethodName = handlerInvocation.MethodName;

                // Step 2: Analyze the handler method for external contract calls
                var externalCalls = FindExternalContractCalls(handlerInvocation.MethodSymbol, model);

                foreach (var externalCall in externalCalls)
                {
                    mappings.Add(new CallMapping
                    {
                        ControllerMethod = controllerMethodName,
                        HandlerMethod = handlerMethodName,
                        CalledExternalMethod = externalCall
                    });
                }
            }
        }

        return mappings;
    }

    private static List<MethodInvocation> FindMethodInvocations(MethodDeclarationSyntax methodDeclaration, SemanticModel model)
    {
        var invocations = new List<MethodInvocation>();

        var invocationExpressions = methodDeclaration.DescendantNodes()
            .OfType<InvocationExpressionSyntax>();

        foreach (var invocation in invocationExpressions)
        {
            var symbolInfo = model.GetSymbolInfo(invocation).Symbol as IMethodSymbol;

            if (symbolInfo != null)
            {
                var methodName = $"{symbolInfo.ContainingType}.{symbolInfo.Name}";
                invocations.Add(new MethodInvocation
                {
                    MethodName = methodName,
                    MethodSymbol = symbolInfo
                });
            }
        }

        return invocations;
    }

    private static List<string> FindExternalContractCalls(IMethodSymbol handlerMethodSymbol, SemanticModel model)
    {
        var externalCalls = new List<string>();

        // Analyze the syntax tree for the handler method's body
        var methodDeclarationSyntax = handlerMethodSymbol.DeclaringSyntaxReferences
            .FirstOrDefault()?.GetSyntax() as MethodDeclarationSyntax;

        if (methodDeclarationSyntax == null)
            return externalCalls;

        var invocationExpressions = methodDeclarationSyntax.DescendantNodes()
            .OfType<InvocationExpressionSyntax>();

        foreach (var invocation in invocationExpressions)
        {
            var symbolInfo = model.GetSymbolInfo(invocation).Symbol as IMethodSymbol;

            if (symbolInfo != null && symbolInfo.ContainingType.TypeKind == TypeKind.Interface)
            {
                var calledMethodName = $"{symbolInfo.ContainingType}.{symbolInfo.Name}";
                externalCalls.Add(calledMethodName);
            }
        }

        return externalCalls;
    }
}

public class CallMapping
{
    public string ControllerMethod { get; set; }
    public string HandlerMethod { get; set; }
    public string CalledExternalMethod { get; set; }
}

public class MethodInvocation
{
    public string MethodName { get; set; }
    public IMethodSymbol MethodSymbol { get; set; }
}
```

### Explanation

1. **Controller-to-Handler Analysis**: 
   - This script first identifies methods in classes that end with "Controller" (typical naming convention for Web API controllers).
   - It then finds the methods invoked within each controller method, which should represent calls to handlers from `AppServices`.

2. **Handler-to-External Contract Analysis**: 
   - For each method called by a controller, it analyzes the implementation of that method to identify any calls to external service contracts (e.g., `DomainN.ServiceN.Contracts.IService`).
   - This is done by traversing the syntax tree of the handler method body to find method invocations that reference interfaces from external assemblies.

3. **Output Construction**: 
   - The `CallMapping` class stores each relationship of interest: which controller method calls which handler method, and which external contract methods are called by the handler.
   - The final output shows the call chain in the format: 
     ```
     Domain.Service.WebApiHost.Controllers.OrderController.PlaceOrder calls Domain.Service.AppServices.OrderHandler.HandlePlaceOrder which calls PaymentService.Contracts.IPaymentService.ProcessPayment
     ```

### How to Run

1. Compile the code into an executable.
2. Run it with paths to the relevant assemblies and source files:
   ```bash
   dotnet run --contracts-path "path/to/Domain.Service.Contracts.dll" --appservices-path "path/to/Domain.Service.AppServices.dll" --webapihost-path "path/to/Domain.Service.WebApiHost.dll" --webapi-source-path "path/to/WebApiHost/Controllers"
   ```

This approach should more accurately reflect the flow of method calls in your microservices architecture, capturing how controllers use handlers, which in turn call external service contracts.




using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.MSBuild;

public class MicroservicesTopologyAnalyzer
{
    public static void Main(string[] args)
    {
        var solutionPath = args[0]; // Path to the .sln or .csproj file of the WebApiHost project
        var contractsPath = args[1]; // Path to Domain.Service.Contracts.dll
        var appServicesPath = args[2]; // Path to Domain.Service.AppServices.dll

        // Load the solution or project using MSBuildWorkspace
        var workspace = MSBuildWorkspace.Create();
        var project = workspace.OpenProjectAsync(solutionPath).Result;

        var references = new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(contractsPath),
            MetadataReference.CreateFromFile(appServicesPath)
        };

        // Analyze the project for method call mappings
        var callMappings = AnalyzeProject(project, references);

        // Output the results
        foreach (var mapping in callMappings)
        {
            Console.WriteLine($"{mapping.ControllerMethod} calls {mapping.HandlerMethod} which calls {mapping.CalledExternalMethod}");
        }
    }

    private static List<CallMapping> AnalyzeProject(Project project, MetadataReference[] references)
    {
        var mappings = new List<CallMapping>();

        foreach (var document in project.Documents)
        {
            var syntaxTree = document.GetSyntaxTreeAsync().Result;
            var compilation = project.GetCompilationAsync().Result.AddReferences(references);
            var model = compilation.GetSemanticModel(syntaxTree);

            // Find all method declarations in controllers
            var controllerMethods = syntaxTree.GetRoot().DescendantNodes()
                .OfType<MethodDeclarationSyntax>()
                .Where(method => method.Parent is ClassDeclarationSyntax classDecl &&
                                 classDecl.Identifier.Text.EndsWith("Controller")); // Filter for controllers

            foreach (var controllerMethod in controllerMethods)
            {
                var controllerMethodSymbol = model.GetDeclaredSymbol(controllerMethod) as IMethodSymbol;
                var controllerMethodName = $"{controllerMethodSymbol.ContainingType}.{controllerMethodSymbol.Name}";

                // Find method invocations from the controller to handlers
                var handlerInvocations = FindMethodInvocations(controllerMethod, model);

                foreach (var handlerInvocation in handlerInvocations)
                {
                    var handlerMethodName = handlerInvocation.MethodName;

                    // Analyze the handler method for external contract calls
                    var externalCalls = FindExternalContractCalls(handlerInvocation.MethodSymbol, model);

                    foreach (var externalCall in externalCalls)
                    {
                        mappings.Add(new CallMapping
                        {
                            ControllerMethod = controllerMethodName,
                            HandlerMethod = handlerMethodName,
                            CalledExternalMethod = externalCall
                        });
                    }
                }
            }
        }

        return mappings;
    }

    private static List<MethodInvocation> FindMethodInvocations(MethodDeclarationSyntax methodDeclaration, SemanticModel model)
    {
        var invocations = new List<MethodInvocation>();

        var invocationExpressions = methodDeclaration.DescendantNodes()
            .OfType<InvocationExpressionSyntax>();

        foreach (var invocation in invocationExpressions)
        {
            var symbolInfo = model.GetSymbolInfo(invocation).Symbol as IMethodSymbol;

            if (symbolInfo != null)
            {
                var methodName = $"{symbolInfo.ContainingType}.{symbolInfo.Name}";
                invocations.Add(new MethodInvocation
                {
                    MethodName = methodName,
                    MethodSymbol = symbolInfo
                });
            }
        }

        return invocations;
    }

    private static List<string> FindExternalContractCalls(IMethodSymbol handlerMethodSymbol, SemanticModel model)
    {
        var externalCalls = new List<string>();

        // Analyze the syntax tree for the handler method's body
        var methodDeclarationSyntax = handlerMethodSymbol.DeclaringSyntaxReferences
            .FirstOrDefault()?.GetSyntax() as MethodDeclarationSyntax;

        if (methodDeclarationSyntax == null)
            return externalCalls;

        var invocationExpressions = methodDeclarationSyntax.DescendantNodes()
            .OfType<InvocationExpressionSyntax>();

        foreach (var invocation in invocationExpressions)
        {
            var symbolInfo = model.GetSymbolInfo(invocation).Symbol as IMethodSymbol;

            if (symbolInfo != null && symbolInfo.ContainingType.TypeKind == TypeKind.Interface)
            {
                var calledMethodName = $"{symbolInfo.ContainingType}.{symbolInfo.Name}";
                externalCalls.Add(calledMethodName);
            }
        }

        return externalCalls;
    }
}

public class CallMapping
{
    public string ControllerMethod { get; set; }
    public string HandlerMethod { get; set; }
    public string CalledExternalMethod { get; set; }
}

public class MethodInvocation
{
    public string MethodName { get; set; }
    public IMethodSymbol MethodSymbol { get; set; }
}
