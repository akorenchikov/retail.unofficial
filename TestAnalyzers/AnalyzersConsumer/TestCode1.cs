namespace Retail.Domain.Api.Contracts
{
    public sealed class ClientContract
    {
        public int AccountID { get; set; }
    }
}
