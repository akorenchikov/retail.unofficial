using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Rename;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(DynamicCodeFixProvider)), Shared]
public class DynamicCodeFixProvider : CodeFixProvider
{
    public sealed override ImmutableArray<string> FixableDiagnosticIds => ImmutableArray.Create("DynamicTransformation");

    public sealed override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

    public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
    {
        var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);
        var diagnostic = context.Diagnostics[0];
        var diagnosticSpan = diagnostic.Location.SourceSpan;

        var diagnosticMessageParts = diagnostic.GetMessage().Split('\'');
        var beforeText = diagnosticMessageParts[1];
        var afterText = diagnosticMessageParts[3];

        if (root.FindToken(diagnosticSpan.Start).Parent.AncestorsAndSelf().OfType<PropertyDeclarationSyntax>().FirstOrDefault() is PropertyDeclarationSyntax property)
        {
            context.RegisterCodeFix(
                CodeAction.Create(
                    title: $"Transform to {afterText}",
                    createChangedSolution: c => ApplyTransformationAsync(context.Document, property, afterText, c),
                    equivalenceKey: "DynamicTransformation"),
                diagnostic);
        }
        else if (root.FindToken(diagnosticSpan.Start).Parent.AncestorsAndSelf().OfType<ClassDeclarationSyntax>().FirstOrDefault() is ClassDeclarationSyntax classDeclaration)
        {
            context.RegisterCodeFix(
                CodeAction.Create(
                    title: $"Transform to {afterText}",
                    createChangedSolution: c => ApplyTransformationAsync(context.Document, classDeclaration, afterText, c),
                    equivalenceKey: "DynamicTransformation"),
                diagnostic);
        }
    }

    private async Task<Solution> ApplyTransformationAsync(Document document, SyntaxNode node, string newName, CancellationToken cancellationToken)
    {
        var semanticModel = await document.GetSemanticModelAsync(cancellationToken);
        var symbol = semanticModel.GetDeclaredSymbol(node, cancellationToken);
        var solution = document.Project.Solution;
        return await Renamer.RenameSymbolAsync(solution, symbol, newName, solution.Workspace.Options, cancellationToken).ConfigureAwait(false);
    }
}
