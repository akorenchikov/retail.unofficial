namespace Retail.Domain.Api.Contracts
{
    public sealed class ClientContractDto
    {
        public int AccountID { get; set; }
    }
}
