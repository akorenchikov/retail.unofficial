using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;

namespace MakeConst {using System.Collections.Immutable;
using System.Composition;
    using System.Linq;
    using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Rename;
using Microsoft.CodeAnalysis.Text;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(PropertyIdCodeFixProvider)), Shared]
public class PropertyIdCodeFixProvider : CodeFixProvider
{
    private const string Title = "Rename to 'Id'";

    public sealed override ImmutableArray<string> FixableDiagnosticIds
    {
        get { return ImmutableArray.Create(PropertyIdAnalyzer.DiagnosticId); }
    }

    public sealed override FixAllProvider GetFixAllProvider()
    {
        return WellKnownFixAllProviders.BatchFixer;
    }

    public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
    {
        var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

        var diagnostic = context.Diagnostics[0];
        var diagnosticSpan = diagnostic.Location.SourceSpan;

        var declaration = root.FindToken(diagnosticSpan.Start).Parent.AncestorsAndSelf()
            .OfType<PropertyDeclarationSyntax>().First();

        context.RegisterCodeFix(
            Microsoft.CodeAnalysis.CodeActions.CodeAction.Create(
                title: Title,
                createChangedSolution: c => RenamePropertyAsync(context.Document, declaration, c),
                equivalenceKey: Title),
            diagnostic);
    }

    private async Task<Solution> RenamePropertyAsync(Document document, PropertyDeclarationSyntax propertyDeclaration, CancellationToken cancellationToken)
    {
        var identifierToken = propertyDeclaration.Identifier;
        var newName = identifierToken.Text.Substring(0, identifierToken.Text.Length - 2) + "Id";

        var semanticModel = await document.GetSemanticModelAsync(cancellationToken);
        var propertySymbol = semanticModel.GetDeclaredSymbol(propertyDeclaration, cancellationToken);
        var solution = document.Project.Solution;

        var optionSet = solution.Workspace.Options;
        var newSolution = await Renamer.RenameSymbolAsync(solution, propertySymbol, newName, optionSet, cancellationToken).ConfigureAwait(false);

        return newSolution;
    }
}

}
