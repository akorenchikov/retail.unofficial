using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Linq;
using System.Diagnostics;

namespace MakeConst {
[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class PropertyIdAnalyzer : DiagnosticAnalyzer
{
    public const string DiagnosticId = "PropertyIdAnalyzer";
    private const string Title = "Property name ends with 'ID'";
    private const string MessageFormat = "Property '{0}' ends with 'ID'";
    private const string Description = "Properties with the postfix 'ID' found in public classes inside the *.Api.Contracts namespace.";
    private const string Category = "Naming";

    private static readonly DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Description);

    public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(Rule); } }

    public override void Initialize(AnalysisContext context)
    {
        context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
        context.EnableConcurrentExecution();
        context.RegisterSyntaxNodeAction(AnalyzeSyntaxNode, Microsoft.CodeAnalysis.CSharp.SyntaxKind.PropertyDeclaration);
    }

    private static void AnalyzeSyntaxNode(SyntaxNodeAnalysisContext context)
    {
        var propertyDeclaration = (Microsoft.CodeAnalysis.CSharp.Syntax.PropertyDeclarationSyntax)context.Node;

        // Check if the property name ends with 'ID'
        if (propertyDeclaration.Identifier.Text.EndsWith("ID"))
        {
            var classDeclaration = propertyDeclaration.Parent as Microsoft.CodeAnalysis.CSharp.Syntax.ClassDeclarationSyntax;
            if (classDeclaration == null)
            {
                return;
            }

            // Check if the class is public
            if (!classDeclaration.Modifiers.Any(SyntaxKind.PublicKeyword))
            {
                return;
            }

            // Check the namespace
            var namespaceDeclaration = classDeclaration.Parent as Microsoft.CodeAnalysis.CSharp.Syntax.NamespaceDeclarationSyntax;
            if (namespaceDeclaration == null || !namespaceDeclaration.Name.ToString().EndsWith(".Api.Contracts"))
            {
                return;
            }

            // Report diagnostic
            var diagnostic = Diagnostic.Create(Rule, propertyDeclaration.GetLocation(), propertyDeclaration.Identifier.Text);
            context.ReportDiagnostic(diagnostic);
        }
    }
}
}
