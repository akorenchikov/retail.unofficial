using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;

public static class SyntaxTreeComparer
{
    private static readonly string[] PatternIdentifiers = { "Namespace1", "Class1", "Property1" };

    public static List<TransformationRule> GenerateTransformationRules(SyntaxNode beforeRoot, SyntaxNode afterRoot)
    {
        var rules = new List<TransformationRule>();

        var beforeProperties = beforeRoot.DescendantNodes().OfType<PropertyDeclarationSyntax>().ToList();
        var afterProperties = afterRoot.DescendantNodes().OfType<PropertyDeclarationSyntax>().ToList();

        for (int i = 0; i < beforeProperties.Count; i++)
        {
            var beforeProperty = beforeProperties[i];
            var afterProperty = afterProperties[i];

            if (!AreIdentifiersMatching(beforeProperty.Identifier.Text, afterProperty.Identifier.Text))
            {
                rules.Add(new TransformationRule
                {
                    Before = beforeProperty.Identifier.Text,
                    After = afterProperty.Identifier.Text,
                    IsClass = false
                });
            }

            if (beforeProperty.Type.ToString() != afterProperty.Type.ToString())
            {
                rules.Add(new TransformationRule
                {
                    Before = beforeProperty.Type.ToString(),
                    After = afterProperty.Type.ToString(),
                    IsClass = false
                });
            }
        }

        var beforeClasses = beforeRoot.DescendantNodes().OfType<ClassDeclarationSyntax>().ToList();
        var afterClasses = afterRoot.DescendantNodes().OfType<ClassDeclarationSyntax>().ToList();

        for (int i = 0; i < beforeClasses.Count; i++)
        {
            var beforeClass = beforeClasses[i];
            var afterClass = afterClasses[i];

            if (!AreIdentifiersMatching(beforeClass.Identifier.Text, afterClass.Identifier.Text))
            {
                rules.Add(new TransformationRule
                {
                    Before = beforeClass.Identifier.Text,
                    After = afterClass.Identifier.Text,
                    IsClass = true
                });
            }
        }

        return rules;
    }

    private static bool AreIdentifiersMatching(string beforeIdentifier, string afterIdentifier)
    {
        if (beforeIdentifier == afterIdentifier)
        {
            return true;
        }

        var postfix = beforeIdentifier
            .Replace("Namespace1", "")
            .Replace("Class1", "")
            .Replace("Property1", "");

        return afterIdentifier.EndsWith(postfix);
    }
}

public class TransformationRule
{
    public string Before { get; set; }
    public string After { get; set; }
    public bool IsClass { get; set; }
}
