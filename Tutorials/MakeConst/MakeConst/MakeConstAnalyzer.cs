﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Linq;
using System.Diagnostics;

namespace MakeConst {
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class MakeConstAnalyzer : DiagnosticAnalyzer
    {
        private static readonly DiagnosticDescriptor Rule = new DiagnosticDescriptor(
        "DynamicTransformation",
        "Dynamic Transformation",
        "Element '{0}' should be transformed to '{1}'",
        "Naming",
        DiagnosticSeverity.Warning,
        isEnabledByDefault: true);

    public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(Rule);

    public override void Initialize(AnalysisContext context)
    {
        context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
        context.EnableConcurrentExecution();
        context.RegisterSyntaxNodeAction(AnalyzeSyntaxNode, SyntaxKind.PropertyDeclaration, SyntaxKind.ClassDeclaration);
    }

    private void AnalyzeSyntaxNode(SyntaxNodeAnalysisContext context)
    {
        var beforeCode = System.IO.File.ReadAllText("path/to/before.cs");
        var afterCode = System.IO.File.ReadAllText("path/to/after.cs");

        var beforeTree = CSharpSyntaxTree.ParseText(beforeCode);
        var afterTree = CSharpSyntaxTree.ParseText(afterCode);

        var beforeRoot = beforeTree.GetRoot();
        var afterRoot = afterTree.GetRoot();

        var rules = SyntaxTreeComparer.GenerateTransformationRules(beforeRoot, afterRoot);

        foreach (var rule in rules)
        {
            Debug.WriteLine(rule);
            if (rule.IsClass && context.Node is ClassDeclarationSyntax classDeclaration)
            {
                if (classDeclaration.Identifier.Text == rule.Before || classDeclaration.Identifier.Text.EndsWith(rule.Before))
                {
                    var diagnostic = Diagnostic.Create(Rule, classDeclaration.Identifier.GetLocation(), rule.Before, rule.After);
                    context.ReportDiagnostic(diagnostic);
                }
            }
            else if (!rule.IsClass && context.Node is PropertyDeclarationSyntax propertyDeclaration)
            {
                if (propertyDeclaration.Identifier.Text == rule.Before || propertyDeclaration.Identifier.Text.EndsWith(rule.Before))
                {
                    var diagnostic = Diagnostic.Create(Rule, propertyDeclaration.GetLocation(), rule.Before, rule.After);
                    context.ReportDiagnostic(diagnostic);
                }
                else if (propertyDeclaration.Type.ToString() == rule.Before)
                {
                    var diagnostic = Diagnostic.Create(Rule, propertyDeclaration.Type.GetLocation(), rule.Before, rule.After);
                    context.ReportDiagnostic(diagnostic);
                }
            }
        }
    }
}
}
